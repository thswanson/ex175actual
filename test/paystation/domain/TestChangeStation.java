package paystation.domain;

import static org.junit.Assert.assertEquals;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.util.Calendar;
import java.util.GregorianCalendar;

import org.junit.Test;

public class TestChangeStation {
	private PayStation ps;
	  
	  @Test 
	  public void testTheEasyStuff() 
	    throws IllegalCoinException {
		  
		  ps = new PayStationImpl( new AlphaTownFactory() );
		    // add $ 2.0: 
		    addOneDollar(); addOneDollar();

		    // In the last configuration, alpha town uses the
		    // end time display
		    int minutes = 200/5*2; /* 80 minutes bought */
		    Calendar now = GregorianCalendar.getInstance();
		    now.add(Calendar.MINUTE, minutes);
		    int result =
		      now.get(Calendar.HOUR_OF_DAY) * 100 
		      + 
		      now.get(Calendar.MINUTE);
		    
		    
		    assertEquals( "Linear Rate: 2$ should give 80 min => end time "+result,
		                  result , ps.readDisplay() );

		    Receipt receipt = ps.buy();
		    // test that a standard receipt is issued.
		    assertEquals( "AlphaTown should use standard receipts",
		                  5, getReceiptLineCount(receipt) );
		    ((PayStationImpl) ps).switchStation( new BetaTownFactory() );
		    // add $ 2.0: 1.5 gives 1 hours, next 0.5 gives 15 min
		    addOneDollar(); addOneDollar();
		    
		    assertEquals( "Progressive Rate: 2$ should give 75 min ",
		                  75 , ps.readDisplay() );

		    Receipt receipt1 = ps.buy();
		    // test that a barcode receipt is issued.
		    assertEquals( "BetaTown should use barcode receipts",
		                  6, getReceiptLineCount(receipt1) );
		    
		   ((PayStationImpl)ps).switchStation(new GammaTownFactory());
		   addOneDollar(); addOneDollar();
		    
		    assertEquals( "WILL FAIL DURING WEEKENDS: Linear rate 2$ = 80 min ",
		                  80 , ps.readDisplay() );
		    
		    Receipt receipt2 = ps.buy();
		    // test that a standard receipt is issued.
		    assertEquals( "GammaTown should use standard receipts",
		                  5, getReceiptLineCount(receipt2) );
		    
	  }
	  
	  @Test
	  public void changeDisplayYo() throws IllegalCoinException {
		  
		  ps = new PayStationImpl( new AlphaTownFactory() );
		    // add $ 2.0: 
		    addOneDollar(); addOneDollar();

		    // In the last configuration, alpha town uses the
		    // end time display
		    int minutes = 200/5*2; /* 80 minutes bought */
		    Calendar now = GregorianCalendar.getInstance();
		    now.add(Calendar.MINUTE, minutes);
		    int result =
		      now.get(Calendar.HOUR_OF_DAY) * 100 
		      + 
		      now.get(Calendar.MINUTE);
		    
		    
		    assertEquals( "Linear Rate: 2$ should give 80 min => end time "+result,
		                  result , ps.readDisplay() );
		    
		    ((PayStationImpl) ps).switchStation( new BetaTownFactory() );
		    
		    assertEquals( "Progressive Rate: 2$ should give 80 min ",
	                  80 , ps.readDisplay() );
		    
		    ((PayStationImpl)ps).switchStation(new GammaTownFactory());
		    
		    Receipt receipt = ps.buy();
		    
		    assertEquals( "GammaTown should use standard receipts",
	                  5, getReceiptLineCount(receipt) );
	  }
	  
	  @Test
	  public void testChangingRatesIGuestt() throws IllegalCoinException {
		  ps = new PayStationImpl( new AlphaTownFactory() );
		    addOneDollar(); 
		    ((PayStationImpl) ps).switchStation( new BetaTownFactory() );
		    addOneDollar();
		    
		    assertEquals( "Progressive Rate: 2$ should give 75 min ",
		                  75 , ps.readDisplay() );
		    
		    ((PayStationImpl) ps).switchStation(new AlphaTownFactory() );
		    addOneDollar(); 
		   // System.out.println(ps.readDisplay());
		    
		    
		    int minutes = 200/5*2 + 40; 
		    Calendar now = GregorianCalendar.getInstance();
		    now.add(Calendar.MINUTE, minutes);
		    int result =
		      now.get(Calendar.HOUR_OF_DAY) * 100 
		      + 
		      now.get(Calendar.MINUTE);
		    assertEquals( "Progressive Rate: 3$ should give" + result,
	                  result , ps.readDisplay() );
		    
		    
	  }
	  
	  private int getReceiptLineCount(Receipt receipt) {
		    ByteArrayOutputStream baos = new ByteArrayOutputStream();
		    PrintStream stream = new PrintStream(baos);
		    receipt.print(stream);
		    String output = baos.toString();
		    String[] lines = output.split("\n");
		    return lines.length;
		  }

		  private void addOneDollar() throws IllegalCoinException {
		    ps.addPayment(25); ps.addPayment(25); 
		    ps.addPayment(25); ps.addPayment(25); 
		  }
}
