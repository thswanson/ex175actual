package paystation.domain;

import org.junit.*;
import static org.junit.Assert.*;

import java.util.*;

/** Test the output calculated from the TimeDisplayStrategy.
 
   This source code is from the book 
     "Flexible, Reliable Software:
       Using Patterns and Agile Development"
     published 2010 by CRC Press.
   Author: 
     Henrik B Christensen 
     Computer Science Department
     Aarhus University
   
   This source code is provided WITHOUT ANY WARRANTY either 
   expressed or implied. You may study, use, modify, and 
   distribute it for non-commercial purposes. For any 
   commercial use, see http://www.baerbak.com/
*/
public class TestTimeDisplay {
  private DisplayStrategy ds;

  @Before public void setUp() {
    ds = new TimeDisplayStrategy();
  }

  /* This test case is a balance as its complexity is actually equal
     to the production code. Another approach would have been to
     introduce a test stub for the system clock which could then be
     set by the JUnit testing code. However, again the cost of this
     operation is higher than the gain in reliability, therefore this
     path was taken. Note also that there is a very small chance that
     this test case actually fails as the time is sampled twice and
     the time for calculating the result below may be a small fraction
     off that of the display strategy. If they sample on different
     sides of the minute boundary it may fail. */
  @Test public void shouldDisplayTimeCorrectly() {
    int minutes = 10; /* 10 minutes bought */
    Calendar now = GregorianCalendar.getInstance();
    now.add(Calendar.MINUTE, 10);
    int result =
      now.get(Calendar.HOUR_OF_DAY) * 100 
      + 
      now.get(Calendar.MINUTE);
    
    assertEquals("10 min from now should be"+result, 
                 result, ds.calculateOutput(10));
  }
} 
